package es.panel.curso.java.proyecto2.alumnos;

public class Alumno extends Persona {

	private boolean experiencia;
	
	/*
	 * CONSTRUCTORES DE LA CLASE
	 */
	
	public Alumno(String nombreCompleto, int edad, boolean experiencia) {
		super(nombreCompleto, edad);
		this.experiencia = experiencia;
		this.experiencia = false;
	}

	/*
	 * METODOS DE LA CLASE
	 */
	
	// ACCEDENTES
	
	
	public boolean isExperiencia() {
		return experiencia;
	}
	
	
	// MUTADORES
	
	public void setExperiencia(boolean experiencia) {
		this.experiencia = experiencia;
	}
	

	// ESPECIFICAS

	public void pintaAlumno() {
		if (experiencia) {
			System.out.println("Alumno con nombre:" + this.nombreCompleto + " y edad: " + this.edad + " experiencia: " + this.experiencia);
		} else {
			System.out.println("Alumno con nombre:" + this.nombreCompleto + " y edad: " + this.edad);
		}
		
	}
	

}
