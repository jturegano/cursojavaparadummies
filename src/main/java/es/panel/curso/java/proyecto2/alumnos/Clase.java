package es.panel.curso.java.proyecto2.alumnos;

import java.util.ArrayList;
import java.util.Iterator;

public class Clase {
	private Tutor tutor;
	private String fecha;
	private ArrayList<Alumno> listAlumno = new ArrayList<>();
	
	public Clase() {
		super();
		this.listAlumno = new ArrayList<>();
	}

	public Clase(Tutor tutor, String fecha) {
		this.tutor = tutor;
		this.fecha= fecha;
		//this.listAlumno = new ArrayList<>();
	}

	public Tutor getTutor() {
		return tutor;
	}

	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public ArrayList<Alumno> getListAlumno() {
		return listAlumno;
	}

	public void setListAlumno(ArrayList<Alumno> listAlumno) {
		this.listAlumno = listAlumno;
	}

	public String getListaAlumnos() {
		String resultado = "Clase=[listAlumno ->(";
		int indice = 0;
		
		for (Alumno alumno : listAlumno) {
			resultado = resultado + alumno.getNombreCompleto();
			if (indice < (listAlumno.size()-1)){
				resultado = resultado + ", ";	
			}
			indice++;		
		}
		
		resultado = resultado + ")]";
		
		return resultado;
	}
	
	public boolean anyadirAlumno (Alumno alumno) {
		return this.listAlumno.add(alumno);
	}

	public boolean eliminarAlumno(Alumno alumno) {
		return this.listAlumno.remove(alumno);		
	}

	public String buscarAlumno(String nombre) {
		String resultado = "";
		int indice = 0;
		
		while (indice < listAlumno.size()) {
			if (nombre.equals(listAlumno.get(indice).getNombreCompleto())){
				resultado = listAlumno.get(indice).getNombreCompleto();	
			}	
			indice++;
			
		}
		
		return resultado;
	}

}
