package es.panel.curso.java.proyecto2.alumnos;

public class Persona {
	protected String nombreCompleto;
	protected int edad;
	
	public Persona(String nombreCompleto, int edad) {
		super();
		this.nombreCompleto = nombreCompleto;
		this.edad = edad;
	}
	
	

	public Persona(String nombreCompleto) {
		super();
		this.nombreCompleto = nombreCompleto;
	}



	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}	
}