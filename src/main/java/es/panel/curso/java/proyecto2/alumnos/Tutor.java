package es.panel.curso.java.proyecto2.alumnos;

public class Tutor extends Persona {
	/*
	 * ATRIBUTOS DE LA CLASE
	 */

	private String nombreProveeedor;

	/*
	 * CONSTRUCTORES DE LA CLASE
	 */
	
	public Tutor(String nombreCompleto, int edad, String nombreProveeedor) {
		super(nombreCompleto, edad);
		this.nombreProveeedor = nombreProveeedor;
	}
	
	/*
	 * METODOS DE LA CLASE
	 */
	
	// ACCEDENTES
	
	public String getNombreProveeedor() {
		return nombreProveeedor;
	}
	
	// MUTADORES

	public void setNombreProveeedor(String nombreProveeedor) {
		this.nombreProveeedor = nombreProveeedor;
	}	

	// ESPECIFICAS

	public void pintaAlumno() {
		System.out.println("Alumno con nombre:" + this.nombreCompleto + " y edad: " + this.edad );		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombreProveeedor == null) ? 0 : nombreProveeedor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tutor other = (Tutor) obj;
		if (nombreProveeedor == null) {
			if (other.nombreProveeedor != null)
				return false;
		} else if (!nombreProveeedor.equals(other.nombreProveeedor))
			return false;
		return true;
	}
	

}
