package es.panel.curso.java.proyecto2.music;

import java.util.ArrayList;
import java.util.Iterator;

public class OrganizadorMusica {
	private ArrayList<String> coleccionMusica;
	private MusicPlayer reproductor;

	public OrganizadorMusica() {
		super();
		this.coleccionMusica = new ArrayList<>();
		this.reproductor = new MusicPlayer();
	}

	public ArrayList<String> getColeccionMusica() {
		return coleccionMusica;
	}

	public void setColeccionMusica(ArrayList<String> coleccionMusica) {
		this.coleccionMusica = coleccionMusica;
	}


	public boolean anyadirCancion(String cancion) {
		return this.coleccionMusica.add(cancion);
	}
	
	public int contarCanciones() {
		return this.coleccionMusica.size();
	}
	
	public String eliminarCancionPorIndice (int indice) {
		String resultado = "valorIndiceInvalido";
		if (esIndiceValido(indice)) {
			resultado = this.coleccionMusica.remove(indice);
		}	
		return resultado;	
	}

	private boolean esIndiceValido(int indice) {
		return indice >= 0 && indice <  (contarCanciones());
	}
	
	public void empezarReproduccion(int indice) {
		this.reproductor.startPlaying(this.coleccionMusica.get(indice));	
	}
	
	public void pararReproduccion() {
		this.reproductor.stop();
	}
	
	public void ejemploReproduccion(int indice) {
		this.reproductor.playSample(this.coleccionMusica.get(indice));	
	}
	
	public void listarCancion(int indice) {
		if (esIndiceValido(indice)) {
			System.out.println(this.coleccionMusica.get(indice));
		} else {
			System.out.println("Indice no valido");
		}
	}
	
	public void listarTodaColeccionMusica() {
		for(String cancion: this.coleccionMusica) {
			System.out.println(cancion);
		}
	}
	
	public void listarTodaColeccionMusicaWhile() {
		int contador = 0;	
		while(contador< contarCanciones()) {
			System.out.println(this.coleccionMusica.get(contador));
			contador++;
		}
	}
	
	public void listarTodaColeccionMusicaIterator() {
		Iterator<String> iterador = this.coleccionMusica.iterator();
		
		while (iterador.hasNext()) {
			System.out.println(iterador.next());		
		}
	}
	
	public void busquedaCancion(String cancion) {
		
	}
	
	
}
