package es.panel.curso.java.proyecto2.alumnos;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class AlumnoTest {

	@Test
	void testConstructorAlumno() {
		Alumno alumno = new Alumno("Belen", 18, true);
		assertEquals(alumno.getNombreCompleto(), "Belen");
		assertEquals(alumno.getEdad(), 18);
		assertEquals(alumno.isExperiencia(), false);
	}
}
