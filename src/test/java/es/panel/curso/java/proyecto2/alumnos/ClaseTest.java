package es.panel.curso.java.proyecto2.alumnos;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
class ClaseTest {
	
	private Clase clase = new Clase();

    @Test
    void listarAlumnos() {
		Alumno ture = new Alumno("Ture", 37, true);
		Alumno belen = new Alumno("Belen", 18, true);	
		clase.anyadirAlumno(ture);
		clase.anyadirAlumno(belen);
				
        assertEquals(clase.getListaAlumnos(), "Clase=[listAlumno ->(Ture, Belen)]");
    }
    
    @Test
    void anyadeAlumno() {
    	Alumno ture = new Alumno("Ture", 37, true);
    	clase.anyadirAlumno(ture);
    	
    	assertEquals(clase.getListAlumno().get(0).getNombreCompleto(), "Ture");
    	assertEquals(clase.getListAlumno().size(), 1);
    	assertEquals(clase.getListAlumno().isEmpty(), false);
    	assertEquals(clase.getListAlumno().contains(ture), true);
    }
    
    @Test
    void eliminaAlumno() {
    	Alumno belen = new Alumno("Ture", 37, true);
    	clase.anyadirAlumno(belen);
    	clase.eliminarAlumno(belen);
    	
    	assertEquals(clase.getListAlumno().isEmpty(), true);
    	assertEquals(clase.getListAlumno().contains(belen), false);
    	assertEquals(clase.getListAlumno().size(), 0);
    }
    
    @Test
    void buscaAlumno() {
    	Alumno belen = new Alumno("Belen", 37, true);
    	clase.anyadirAlumno(belen);
    	
    	assertEquals(clase.buscarAlumno("Belen"), "Belen");
    	
    	
    	
    }
   
}
